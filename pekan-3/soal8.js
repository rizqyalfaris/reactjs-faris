function sum(...restFunction) {
    let result = 0;
    for (const num of restFunction) {
      result += num;
    }
    return result;
  };
  
  let result1 = sum(2, 4, 6);
  let result2 = sum(5, 2, 7, 4, 8);

  console.log(result1);
  console.log(result2);