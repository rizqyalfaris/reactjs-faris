const arrObj = [
    {
        name: "Bakso",
        price: 15000,
        rating: 7,
    },
    {
        name: "Mie Ayam",
        price: 18000,
        rating: 8.5,
    },
    {
        name: "Bubur Ayam",
        price: 10000,
        rating: 6,
    },
    {
        name: "Ketoprak",
        price: 12000,
        rating: 5,
    },
    {
        name: "Nasi Padang",
        price: 20000,
        rating: 9,
    },
];

const resArrObj = arrObj.filter((item, index) => {
    return item.rating > 7;
})
console.log(resArrObj)