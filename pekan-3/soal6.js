const arr = [5, 3, 4, 7, 6, 9, 2];

const mapResult = arr.map((item, index) => {
    if (index % 2 == 0) {
        return item * 2;
    } else {
        return item * 3;
    }
});
console.log(mapResult);