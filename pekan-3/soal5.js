const arrObj = [
    {
        name: "Headset",
        price: 250000,
    },
    {
        name: "Handphone",
        price: 150000,
    },
    {
        name: "Laptop",
        price: 850000,
    },
];

const mapResult = arrObj.map((item, index) => {
    return item.price;
});
console.log(mapResult)