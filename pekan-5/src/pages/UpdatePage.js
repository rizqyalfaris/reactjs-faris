import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Table from "../components/Table";
import CreateForm from "../components/CreateForm";
import UpdateForm from "../components/UpdateForm";
import Footer from "../components/Footer";
import Layout from "../components/layout/Layout";
import { Helmet } from "react-helmet";

function UpdatePage() {

  return (
    <div>
    <Helmet>
      <title>Update Product Page</title>
    </Helmet>
    <Layout>
      <UpdateForm />
    </Layout>
    </div>
  );
}

export default UpdatePage;
