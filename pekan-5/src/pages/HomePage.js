import React, { useContext, useEffect } from "react";
import { Helmet } from "react-helmet";
import Navbar from "../components/Navbar";
import Product from "../components/Product";
import { GlobalContext } from "../context/GlobalContext";
import Footer from "../components/Footer";
import Layout from "../components/layout/Layout";

function HomePage() {
    const { product, fetchProducts, editProduct, setEditProduct, loading } = useContext(GlobalContext);

    useEffect(() => {
        fetchProducts();
      }, []);

  return (
    <div>
    <Helmet>
      <title>Home Page</title>
    </Helmet>
      <Layout>
      <div className="max-w-5xl mx-auto my-10">
        <h1 className="font-bold text-2xl text-cyan-600 mx-8 my-8 mb-4">
          Catalog Products
        </h1>
        <div className="flex-col: flex-direction: column mx-8 my-8">
          {product.map((product) => (
            <Product key={product.id} product={product} />
          ))}
        </div>
      </div>
      </Layout>
    </div>
  );
}

export default HomePage;
