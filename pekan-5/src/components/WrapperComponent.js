import React from 'react'

function WrapperComponent({children}) {
  return (
    <div className='max-w-5xl mx-auto bg-red-600 rounded-lg p-10 my-6'>
        <h1 className='text-white text-4xl text-center my-8'>
            Ini text yang bersalah dari Wrapper Component
        </h1>
        {children}
    </div>
  )
}

export default WrapperComponent