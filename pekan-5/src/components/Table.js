import axios from "axios";
import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { Link } from "react-router-dom";

function Table() {
  const { product, fetchProducts, setEditProduct } = useContext(GlobalContext);
  const onDelete = async (id) => {
    try {
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      fetchProducts();
      alert("Berhasil menghapus product");
    } catch (error) {
      alert("Terjadi sesuatu error!");
    }
  };
  return (
    <section>
      <h1 className="my-16 text-3xl font-bold text-center">Table Products</h1>
      <div className="max-w-7xl mx-auto w-full">
        <table className="border border-gray-500 w-full">
          <tr>
            <th className="border border-gray-500 p-2">ID</th>
            <th className="border border-gray-500 p-2">Nama</th>
            <th className="border border-gray-500 p-2">Status Diskon</th>
            <th className="border border-gray-500 p-2">Harga</th>
            <th className="border border-gray-500 p-2">Harga Diskon</th>
            <th className="border border-gray-500 p-2">Gambar</th>
            <th className="border border-gray-500 p-2">Kategori</th>
            <th className="border border-gray-500 p-2">Action</th>
          </tr>
          {product.map((product, index) => {
            return (
              <tr key={product.id}>
                <td className="border border-gray-500 p-2 text-center">
                  {product.id}
                </td>
                <td className="border border-gray-500 p-2 text-center">
                  {product.name}
                </td>
                <td className="border border-gray-500 p-2 text-center">
                  {product.is_diskon === true ? "Aktif" : "Tidak Aktif"}
                </td>
                <td className="border border-gray-500 p-2 text-center">
                  {product.harga}
                </td>
                <td className="border border-gray-500 p-2 text-center">
                  {product.harga_diskon}
                </td>
                <td className='border border-gray-500 p-2 justify-center align="center"'>
                  <img
                    src={product.image_url}
                    alt=""
                    className='w-64 justify-center align="center"'
                  />
                </td>
                <td className="border border-gray-500 p-2 text-center">
                  {product.category}
                </td>
                <td className="border border-gray-500 p-2 justify-center items-center">
                  <div className="flex gap-2 justify-center items-center">
                    <Link to={`/update/${product.id}`}>
                      <button className="px-3 py-1 bg-yellow-600 text-white">
                        Update
                      </button>
                    </Link>
                    <button
                      onClick={() => {
                        // console.log(product.id);
                        onDelete(product.id);
                      }}
                      className="px-3 py-1 bg-red-600 text-white"
                    >
                      Delete
                    </button>
                  </div>
                </td>
              </tr>
            );
          })}
        </table>
      </div>
    </section>
  );
}

export default Table;
