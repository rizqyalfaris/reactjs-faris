import React from 'react'

function Product({ product }) {
    return (
        <div className="mt-4 grid md: grid-cols-4 pb-10">
            <div className="border-solid border-2 border-sky-500 rounded-lg bg-white">
                <img src={product.image_url} className="rounded-lg h-80 w-96" />
                <label className="p-2">{product.name}</label><br />
                <label className="p-2 line-through">Rp {product.harga}</label><br />
                <label className="p-2 text-orange-500">Rp {product.harga_diskon}</label><br/>
                <label className="p-2 text-cyan-500">Stock {product.stock}</label>
            </div>
        </div>
    )
}

export default Product