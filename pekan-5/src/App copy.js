import axios from "axios";
import { useContext, useEffect, useState } from "react";
import "./App.css";
import Product from "./components/Product";
import CreateForm from "./components/CreateForm";
import Table from "./components/Table";
import UpdateForm from "./components/UpdateForm";
import { GlobalContext } from "./context/GlobalContext";

function App() {
  const { product, fetchProducts, editProduct, setEditProduct, loading } = useContext(GlobalContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className="mx-auto my-8">
      {editProduct === null ? (
        <CreateForm />
      ) : (
        <UpdateForm editProduct={editProduct} setEditProduct={setEditProduct} />
      )}

      {loading === true ? (
        <h1 className="text-center text-3xl font-bold">Loading...</h1>
      ) : (
        <div>
          <Table setEditProduct={setEditProduct} />
          <h1 className="font-bold text-2xl text-cyan-600 mx-8 my-8">
            Catalog Products
          </h1>
          <div className="mx-8 my-8">
            {product.map((product) => (
              <Product key={product.id} product={product} />
            ))}
          </div>
        </div>
      )}

      <div>
        <Table setEditProduct={setEditProduct} />
        <h1 className="font-bold text-2xl text-cyan-600 mx-8 my-8">
          Catalog Products
        </h1>
        <div className="mx-8 my-8">
          {product.map((product) => (
            <Product key={product.id} product={product} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
